﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StringCalculator
{
    public class StringKata
    {
        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            var stringNumbersArray = SplitStringNumbersWithDelimiters(numbers);
            var numbersList = FilterOutNumbersGreaterThanThousand(stringNumbersArray);
            
            return Sum(numbersList);
        }

        private string[] SplitStringNumbersWithDelimiters(string numbers)
        {   
            const string forwardSlashes = "//"; 
            char[] delimiters = { ',','\n' };
            
            if (numbers.StartsWith(forwardSlashes))
            {
                return SplitNumbersWithCustomDelimiters(numbers);
            }
            
            return numbers.Split(delimiters);
        }
        private string[] SplitNumbersWithCustomDelimiters(string numbers)
        {   
            const string newlineDelimiter = "\n";
            const string forwardSlashes = "//";
            const string emptyString = "";
            char[]  bracketsArray = { '[', ']' };

            var numbersAndDelimiters = numbers.Split(newlineDelimiter);
            numbersAndDelimiters[0] = numbersAndDelimiters[0].Replace(forwardSlashes, emptyString);

            if (numbersAndDelimiters[0].StartsWith(bracketsArray[0]) && numbersAndDelimiters[0].EndsWith(bracketsArray[1]))
            {
                var delimiters = numbersAndDelimiters[0].Split(bracketsArray, StringSplitOptions.RemoveEmptyEntries);
                return numbersAndDelimiters[1].Split(delimiters, StringSplitOptions.None);
            }
            
            return numbersAndDelimiters[1].Split(numbersAndDelimiters[0], StringSplitOptions.None);
        }
        
        private List<int> FilterOutNumbersGreaterThanThousand(string[] stringNumbersArray)
        {
            var numbersListInRange = new List<int>();
            var intNumbersArray = ConvertStringArrayToIntegerArray(stringNumbersArray);

            CheckForNegativeNumbers(stringNumbersArray);

            foreach(var number in intNumbersArray.Where(value => value < 1001 && value > 0))
            {
                numbersListInRange.Add(number);
            }

            return numbersListInRange;
        }
        private void CheckForNegativeNumbers(string[] stringNumbersArray)
        {
            const string separator = ",";
            const string exceptionMessage = "Negatives not allowed: ";
            var negativeNumbersList = new List<int>();
            var intNumbersArray = ConvertStringArrayToIntegerArray(stringNumbersArray);

            foreach(var number in intNumbersArray.Where(value => value < 0))
            { 
                negativeNumbersList.Add(number);
            }

            if (negativeNumbersList.Any())
            {
               throw new Exception(exceptionMessage + string.Join(separator, negativeNumbersList));
            }
        }

        private int[] ConvertStringArrayToIntegerArray(string[] stringNumbersArray)
        {
            var intNumbersArray = new int[stringNumbersArray.Length];
            var counter = 0;

            foreach(var number in stringNumbersArray)
            {
                var convertedNumber = int.Parse(number);
                intNumbersArray[counter] = convertedNumber;
                counter++;
            }

            return intNumbersArray;
        }

        private int Sum(List<int> numbersList)
        {
            return numbersList.Sum();
        }
    }
}
